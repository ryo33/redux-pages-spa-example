import React from 'react';                     
import { connect } from 'react-redux';
import { SWITCH } from './actions.js';

const mapDispatchToProps = {
  handleClick: () => ({type: SWITCH})
}

const BigButton = ({ handleClick }) => {
  return (
    <div>
      <button onClick={handleClick}>Big Button</button>
    </div>
  );
};

export default connect(null, mapDispatchToProps)(BigButton);
