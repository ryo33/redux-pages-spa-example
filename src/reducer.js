import { combineReducers } from 'redux';
import { createPagesReducer } from 'redux-pages';
import { taskPage } from './pages.js';
import { DONE, RESET } from './actions.js';

// Create a reducer which stores a page to show
const pageReducer = createPagesReducer(taskPage.name, {});

// Create a reducer which stores the number of done tasks
const doneReducer = (state = 0, action) => {
  switch (action.type) {
    case DONE:  return state + 1
    case RESET: return 0
    default:    return state
  }
};

// Create the final reducer.
const reducer = combineReducers({
  page: pageReducer,
  done: doneReducer
});

export default reducer;
