import {
  createMiddleware, composeMiddleware
} from 'redux-middlewares';
import { SWITCH, RESET } from './actions.js';
import { taskPage, relaxPage } from './pages.js';
import { doneSelector, pageSelector } from './selectors.js';

// Switch 'on' or 'off'
const switchMiddleware = createMiddleware(
  SWITCH, // When the SWITCH action is given
  ({ getState, dispatch, nextDispatch, action }) => {
    nextDispatch(action);
    switch (pageSelector(getState()).name) {
      case relaxPage.name:
        dispatch(taskPage.action());
        break;
      case taskPage.name:
        dispatch(relaxPage.action());
        break;
      default:
        break;
    }
  }
);

// Check whether the number of done tasks is one or more, or not
const checkDoneTasksMiddleware = createMiddleware(
  // When the relaxPage action is given
  ({ action }) => relaxPage.check(action),
  // When the number of done tasks is 0
  ({ getState }) => doneSelector(getState()) === 0,
  // Transition to 'on' page
  ({ dispatch }) => dispatch(taskPage.action())
);

// Reset the number of done tasks when switch to relax page.
const resetDoneTasksMiddleware = createMiddleware(
  ({ action }) => relaxPage.check(action), // When the relaxPage action is given
  ({ dispatch, nextDispatch, action }) => {
    nextDispatch(action);
    dispatch({type: RESET});
  }
);

export default composeMiddleware(
  switchMiddleware,
  checkDoneTasksMiddleware,
  resetDoneTasksMiddleware
);
