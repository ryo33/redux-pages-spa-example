import React from 'react';                     
import { connect } from 'react-redux';
import { doneSelector } from './selectors.js'
import { DONE } from './actions.js'
import BigButton from './BigButton.js'

const mapStateToProps = state => {
  const done = doneSelector(state);
  return { done };
};

const mapDispatchToProps = {
  handleClick: () => ({type: DONE})
}

const TaskPage = ({ done, handleClick }) => {
  return (
    <div>
      <p>Do your tasks.</p>
      <p>DONE: {done} task(s)</p>
      <button onClick={handleClick}>I finished one task.</button>
      <BigButton />
    </div>
  );
};

export default connect(mapStateToProps, mapDispatchToProps)(TaskPage);
