import { createPages } from 'redux-pages';

// Define pages
export const pages = createPages();
export const taskPage = pages.addPage('/on', 'on');
export const relaxPage = pages.addPage('/off', 'off');
export const otherPages = pages.addPage('/*', 'error');
