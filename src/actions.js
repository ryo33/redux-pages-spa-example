// Increase the number of done tasks
export const DONE = 'DONE';
// Reset the number
export const RESET = 'RESET';
// Switch 'on' and 'off'
export const SWITCH = 'SWITCH';
