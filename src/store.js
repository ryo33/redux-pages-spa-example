import { createStore, applyMiddleware } from 'redux';
import createHistory from 'history/createBrowserHistory';
import { pageSelector } from './selectors.js';
import { pages } from './pages.js';
import reducer from './reducer.js';
import middleware from './middleware.js';

// Create a history object
const history = createHistory();
// Create a function to get a current path
const getCurrentPath = () => history.location.pathname;
// Create a function to push a next path
const pushPath = (path) => history.push(path);
// Create a redux-pages middleware
const pagesMiddleware = pages.middleware(pageSelector, getCurrentPath, pushPath);

// Create a store
const store = createStore(
  reducer,
  applyMiddleware(pagesMiddleware, middleware)
);
// Apply the current path
pages.handleNavigation(store, history.location.pathname);
// Listen the history updates.
history.listen((location, action) => {
  pages.handleNavigation(store, location.pathname)
});

export default store;
