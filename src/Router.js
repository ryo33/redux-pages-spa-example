import React from 'react';                     
import { connect } from 'react-redux';
import { pageSelector } from './selectors.js';
import { taskPage, relaxPage } from './pages.js';
import TaskPage from './TaskPage.js';
import RelaxPage from './RelaxPage.js';
import ErrorPage from './ErrorPage.js';

const mapStateToProps = state => {
  const page = pageSelector(state);
  return page;
};

const Router = page => {
  switch (page.name) {
    case taskPage.name:
      return <TaskPage />;
    case relaxPage.name:
      return <RelaxPage />;
    default:
      return <ErrorPage />;
  }
};

export default connect(mapStateToProps)(Router);
